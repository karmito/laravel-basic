<?php

	require __DIR__ .'/vendor/autoload.php';
	$log = new Monolog\Logger('myapp');
	$log->pushHandler(new Monolog\Handler\StreamHandler('app.log',Monolog\Logger::INFO));
	$log->addInfo('Program started');