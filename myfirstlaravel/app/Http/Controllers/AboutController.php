<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
    	$data = [
    		"name"=>"Herry Karmito",
    		"email"=>"karmito.herry@gmail.com",
    		"favorites"=>[]//["food","coffee","tea"]
    	];
    	return view('about', $data);
    }
}
